class Blog < ApplicationRecord
  has_many :authors
  has_many :users, through: :authors
  has_many :blog_languages
  has_many :languages, through: :blog_languages
  has_many :posts
end
