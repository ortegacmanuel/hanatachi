class Post < ApplicationRecord
  belongs_to :blog
  belongs_to :language
  belongs_to :author
end
