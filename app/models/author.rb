class Author < ApplicationRecord
  belongs_to :user
  belongs_to :blog
  has_many :posts
end
