class Language < ApplicationRecord
  has_many :blog_languages
  has_many :blogs, through: :blog_languages
  has_many :posts
end
