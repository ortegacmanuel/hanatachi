Rails.application.routes.draw do
  devise_for :users
  root to: "home#index"

  get 'article', to: "home#article"
end
