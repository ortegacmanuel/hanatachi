puts "Seeding..."

esperanto = Language.create! name: "Esperanto", code: "eo"
spanish = Language.create! name: "Español", code: "es"

codigo_nomada_blog = Blog.create!(
  name: "Código Nómada",
  description: "Programación, idiomas y vida nómada"
  slug: "codigo-nomada"
)
codigo_nomada_blog.blog_languages.create! language: esperanto
codigo_nomada_blog.blog_languages.create! language: spanish

manuel_user = User.create! email: "manuel@hantachi.com", password: "123456"
manuel_author = Author.create! user: manuel_user, blog: codigo_nomada_blog

first_post = Post.create!(
  title: "Hola, mundo!",
  body: "Hola, mundo!",
  author: manuel_author,
  language: spanish,
  blog: codigo_nomada_blog
)

second_post = Post.create!(
  title: "Saluton, mondo!",
  body: "Saluton, mondo!",
  author: manuel_author,
  language: esperanto,
  blog: codigo_nomada_blog
)

puts "Seeding done."
