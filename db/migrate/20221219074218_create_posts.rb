class CreatePosts < ActiveRecord::Migration[7.0]
  def change
    create_table :posts do |t|
      t.string :title
      t.text :body

      t.belongs_to :blog
      t.belongs_to :author
      t.belongs_to :language

      t.timestamps
    end
  end
end
