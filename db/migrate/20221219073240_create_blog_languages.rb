class CreateBlogLanguages < ActiveRecord::Migration[7.0]
  def change
    create_table :blog_languages do |t|
      t.belongs_to :blog
      t.belongs_to :language

      t.timestamps
    end
  end
end
